package com.example.practica0193;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button btnSaludar, btnLimpiar, btnCerrar;
    private TextView lblSaludar;
    private EditText txtNombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSaludar = (Button) findViewById(R.id.btnSaludar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        lblSaludar = (TextView) findViewById(R.id.lblSaludo);
        txtNombre = (EditText) findViewById(R.id.txtSaludo);

        btnSaludar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(txtNombre.getText().toString().matches("")) {
                    // Faltó capturar el nombre
                    Toast.makeText(MainActivity.this,
                            "Favor de ingresar el nombre",
                            Toast.LENGTH_SHORT).show();
                }
                else {
                    String saludar = txtNombre.getText().toString();
                    lblSaludar.setText("Hola " + saludar + " ¿Como estas?");
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtNombre.setText("");
                lblSaludar.setText("");
                txtNombre.requestFocus();
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}